# template-nextjs

This is a template project of Next.JS application using basic and necessary features to build a modern web-site.

## Contents :star:

Here are versions of project: 

1. **React 19 - Next.JS 15** (using Pages Router);
2. **React 18 - Next.JS 14** (using App Router);
3. **React 17 - Next.JS 12** (using Pages Router);
4. **React 17 - Next.JS 12** (static site generation);

The latter template is **React 19 - Next.JS 15**. Older versions here are for archive & examples which might be useful.

- **The latter template utilizes**:

  - [Server-Side-Rendering](https://nextjs.org/docs/pages/building-your-application/rendering/server-side-rendering);

  - [Pages Router](https://nextjs.org/docs/pages/building-your-application/routing/pages-and-layoutss);

    > :question: **Why Pages Router was chosen while Next.JS mostly advices to use App Router?**
    >
    > From the experience perspective Pages Router:
    >
    > 1. Provides more control to the flow: you specify `_app.tsx`, `_document.tsx` where you can add some custom behavior;
    > 2. API endpoints are more common: `handler()` for all instead of pre request-method `GET()`/`POST()`, convenient when proxying requests;
    > 3. It is possible to reverse-proxy to URLs with self-signed certificates. Built-in both
    >    `next.config.mjs -> rewrites` ([see](https://nextjs.org/docs/pages/api-reference/config/next-config-js/rewrites))
    >    `middleware.ts -> NextResponse.rewrite()` ([see](https://nextjs.org/docs/pages/api-reference/functions/next-response#rewrite)) 
    >    can't handle self-signed certificates easily.

  - [Per-page layouts](https://nextjs.org/docs/pages/building-your-application/routing/pages-and-layouts#per-page-layouts): specify different layouts for some pages;

  - [Custom errors](https://nextjs.org/docs/pages/building-your-application/routing/custom-error): customize not-found and server-error pages;

  - [Middleware](https://nextjs.org/docs/pages/building-your-application/routing/middleware): tweak request pipeline;

  - [API](https://nextjs.org/docs/pages/building-your-application/routing/api-routes): build simple API-endpoints at front-end app;

  - [Scripts](https://nextjs.org/docs/pages/api-reference/components/script): add 3rd-party scrips;

  - [Sass CSS preprocessor](https://nextjs.org/docs/pages/building-your-application/styling/sass);

  - [Localization](https://next-intl.dev/docs/getting-started/pages-router);

  - [ESLint](https://nextjs.org/docs/pages/api-reference/config/eslint);

- **The latter template includes**:

  - Environment-dependent configuration;
  - Pure form elements (button, inputs);
  - Proxy requests as example from `/users` to `https://gitlab.com/api/v4/users`;
  - SEO: OpenGraph info for links preview;
  - SEO: `link rel="alternate"` for other localizations of a page;
  - Accessibility: "*Skip to main content*" link;
  - Preview for components at `/gallery` page, which is available only at DEV environment;
  - Placeholder for Google Analytics scripts;
  - Placeholder for unit-tests;

## Getting Started :rocket:

1. Switch to version sub-folder;

2. Install packages:

   ```bash
   npm install
   ```

3. Start project for development:

   ```bash
   npm run dev
   ```

4. Testing:

   ```bash
   npm run lint
   npm run test
   ```

5. Build and view production:

   ```bash
   npm run build
   npm run start
   ```

## TODO :bulb:

- [x] Add `CONTRIBUTING.md`;
- [ ] Add form submission example utilizing `useAsync()` hook from lib to the latest template (**blocker**: create lib);
- [ ] Provide styling customizations to `@/components/form`;
- [ ] Implement dropdown `select` form component (**optional**: extract to lib);
- [ ] Implement `validated` components on top of `@/components/form` (**optional**: extract to lib);
- [ ] Add example of fetching data: on server-side via `getServerSideProps()` & on client-site via API-service (**blocker**: API-service is from lib);
