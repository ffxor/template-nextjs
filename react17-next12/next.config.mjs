import { join, dirname } from "path";
import { fileURLToPath } from "url";

const __dirname = dirname(fileURLToPath(import.meta.url));
const languages = process.env.NEXT_PUBLIC_LANGUAGES?.split(",") || [];

const nextConfig = {
    distDir: "dist",
    poweredByHeader: false,
    // Configure the Sass compiler
    sassOptions: {
        includePaths: [join(__dirname, "src/styles")]
    },
    i18n: {
        locales: ["default", ...languages],
        // To redirect from "/" to "/en" in middleware
        // https://nextjs.org/docs/pages/building-your-application/routing/internationalization#prefixing-the-default-locale
        defaultLocale: "default",
        localeDetection: false,
    },
    // Crucial to add for locale being redirected from "/" (aka "default") to "/en" to work
    trailingSlash: true
}

export default nextConfig;