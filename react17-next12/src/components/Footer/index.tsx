import cn from "classnames";

import { IComponentProps } from "@/interfaces";

import styles from "./Footer.module.scss";

export function Footer({ className }: IComponentProps) {

    return (
        <footer className={cn(styles.footer, className)}>
            <p>Footer</p>
        </footer>
    );
}