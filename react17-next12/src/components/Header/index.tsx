import Link from "next/link";
import cn from "classnames";
import { useTranslations } from "next-intl";

import { IComponentProps } from "@/interfaces";
import { NavLink } from "@/components/NavLink";
import cfg from "@/configuration";

import styles from "./Header.module.scss";

export function Header({ className }: IComponentProps) {

    const t = useTranslations();

    return (
        <header className={cn(styles.header, className)}>
            <nav>
                <ul className={cn(styles.header__menu)}>
                    <li>
                        <Link href="/">
                            <a className="link">{t("Home")}</a>
                        </Link>
                    </li>
                    <li><NavLink href="/about" title={t("About.Hint")}>{t("About.Title")}</NavLink></li>
                    <li><NavLink href="/blog" title={t("Blog.Hint")}>{t("Blog.Title")}</NavLink></li>
                    <li><NavLink href="/form" title={t("Form.Hint")}>{t("Form.Title")}</NavLink></li>
                    <li><NavLink href="/api" target="_blank" title="API example">API</NavLink></li>
                    <li><NavLink href={new Date().getDate().toString()} title="Random link to display Not Found">Not Found</NavLink></li>
                </ul>
            </nav>
            <LanguageSwitcher languages={cfg.languages} />
        </header>
    );
}

function LanguageSwitcher({ languages }: { languages: Array<string> }) {

    return (
        <nav>
            <ul className={cn(styles.header__menu)}>
                {languages.map(item =>
                    <li key={item}>
                        <Link href="" locale={item}>{item.toUpperCase()}</Link>
                    </li>
                )}
            </ul>
        </nav>
    );
}