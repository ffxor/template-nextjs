"use client";

import Link from "next/link";
import { useRouter } from "next/router";

export function NavLink({ href, children, ...rest }: React.AnchorHTMLAttributes<HTMLAnchorElement>) {

    const { pathname: path } = useRouter();

    return (
        <Link href={href || "#"}>
            <a className={new RegExp(`^${href}\/?`).test(path) ? "link link_active" : "link"} {...rest}>{children}</a>
        </Link>
    );
}