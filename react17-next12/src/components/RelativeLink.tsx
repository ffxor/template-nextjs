"use client";

import Link from "next/link";
import { useRouter } from "next/router";

export function RelativeLink({ href, children, ...rest }: React.AnchorHTMLAttributes<HTMLAnchorElement>) {

    const { pathname: path } = useRouter();

    return (
        <Link href={`${path}/${href}`}>
            <a className="link" {...rest}>{children}</a>
        </Link>
    );
}