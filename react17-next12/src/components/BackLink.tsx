import Link from "next/link";
import { ReactNode } from "react";
import { PropsWithChildren } from "react";

export function BackLink({ children }: PropsWithChildren<ReactNode>) {
    return (
        <Link href=".">
            <a className="link">{children}</a>
        </Link>
    );
}