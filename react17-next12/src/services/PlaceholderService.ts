"use server";

import { FormEvent } from "react";

export type Post = {
    id: string,
    title: string,
    body: string
}

export type User = {
    id: number,
    name: string
}

export async function GetAllPosts(): Promise<Post[]> {

    const URL = "https://jsonplaceholder.typicode.com/posts";

    const response = await fetch(URL);
    const posts: Post[] = (await response.json()).slice(0, 10); // Take first 10 elements for brevity

    return posts;
}

export async function GetPost(id: string): Promise<Post | null> {

    const URL = `https://jsonplaceholder.typicode.com/posts/${id}`;

    const response = await fetch(URL);
    if (response.status === 404) {
        return null;
    }
    const post: Post = await response.json();

    return post;
}

export async function GetUsers() {

    const URL = "https://jsonplaceholder.typicode.com/users";

    const response = await fetch(URL);
    const posts: User[] = (await response.json()).slice(0, 10); // Take first 10 elements for brevity

    return posts;
}

export async function SubmitForm(signal: AbortSignal, event: FormEvent<HTMLFormElement>) {

    event.preventDefault();
    const form = event.target as HTMLFormElement;
    const data = new FormData(form);

    const response = await fetch(form.action, {
        method: "POST",
        body: data,
        signal: signal
    });

    if (signal.aborted) {
        throw new Error("Aborted");
    }
    const result = await response.text();
    if (!response.ok) {
        throw new Error(result);
    }

    return result;
}