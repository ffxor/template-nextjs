import { ReactNode } from "react";
import { AbstractIntlMessages } from "next-intl";

export interface IComponentProps {
    // Provides classname to be set from parent component
    className?: string,
    children?: ReactNode | undefined
}

export interface IPageProps {
    pageProps: {
        // Translations source
        translations: AbstractIntlMessages | undefined
    }
}

export interface ILocalizationContext {
    locales: Array<string>,
    locale: string,
    defaultLocale: string
}