import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(request: NextApiRequest, response: NextApiResponse<string>) {

    const data = request.body;
    console.log(data);

    // A synthetic delay for loader element to be shown
    await new Promise(resolve => setTimeout(resolve, 2000));

    data.toString().includes("error")
        ? response.status(400).send("Bad request, man")
        : response.status(200).send("Form accepted at " + new Date().toLocaleTimeString());
}