import type { NextApiRequest, NextApiResponse } from "next";

export default function handler(_request: NextApiRequest, response: NextApiResponse<string>) {

    response.status(200).send("Hello Api!");
}