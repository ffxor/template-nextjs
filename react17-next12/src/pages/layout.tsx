import { PropsWithChildren, ReactNode } from "react";
import Head from "next/head";


import { Footer } from "@/components/Footer";
import { Header } from "@/components/Header";

export default function Layout({ children }: PropsWithChildren<ReactNode>) {

    return (
        <>
            <Head>
                <title>Hello Next 12 application</title>
                <meta name="description" content="This is page desription for SEO" />
            </Head>
            <Header />
            <main>{children}</main>
            <Footer />
        </>
    );
}