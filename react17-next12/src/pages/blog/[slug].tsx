import { useRouter } from "next/router";
import Head from "next/head";

import { BackLink } from "@/components/BackLink";
import { GetPost, Post } from "@/services/PlaceholderService";
import { getTranslations } from "@/helpers";
import { ILocalizationContext } from "@/interfaces";

// Here comes context-object with params-prop, containing key-value pairs from request 
// And request is "/blog/1" which gives (params={slug: "1"}), thus (slug="1")
export async function getStaticProps({ params: { slug }, locale }:
    { params: { slug: string } }
    & ILocalizationContext) {

    const post = await GetPost(slug);

    if (!post) {
        return {
            // Causes 404 page
            // It makes sense with "fallback: true" at getStaticPaths()
            notFound: true
        };
    }

    return {
        props: {
            post,
            // NB! with page router there is an issue with using async pages
            // Till data won't be fetched, translations also not coming to the page, if the page isn't pre-rendered with getStaticPaths()
            ...await getTranslations(locale)
        }
    };
}

// By default, Next.js pre-generates all pages, but for dynamic ones like [pid].js it doesn't know, 
// with which keys it should be pre-generated, since getStaticProps() is used
export async function getStaticPaths({ locales }: { locales: Array<string> }) {

    // Pre-generate highly-visited pages for all locales
    // Let's assume it is the furst one in this example.
    // Others - will have Loading-placeholder before
    const params = locales.map(l => {
        return { params: { slug: "1" }, locale: l };
    });

    return {
        paths: [
            ...params
        ],
        // Allows to navigate to not pre-generateg slugs (other than "1")
        fallback: true
    };
}

export default function BlogPost({ post }: { post: Post }) {

    const { query: { slug } } = useRouter();

    return (
        <>
            <Head>
                <title>Blog post - {slug}</title>
            </Head>
            <section className="section">
                <h1>Blog post: {slug}</h1>
                {!post
                    ? <i>Loading post...</i>
                    : <article>
                        <h2>{post.title}</h2>
                        <p>{post.body}</p>
                    </article>
                }
                <p></p>
                <BackLink>Back</BackLink>
            </section>
        </>
    );
}