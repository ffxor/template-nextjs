import Head from "next/head";
import type { AppProps } from "next/app";
import { useRouter } from "next/router";
import { NextIntlClientProvider } from "next-intl";

import Layout from "./layout";
import icon from "@/assets/icon.svg";
import { IPageProps } from "@/interfaces";
import cfg from "@/configuration";

import "@/styles/index.scss";

export default function MyApp({ Component, pageProps }: AppProps & IPageProps) {

    const { locale, pathname } = useRouter();

    return (
        <NextIntlClientProvider locale={locale} timeZone={cfg.timeZone} messages={pageProps.translations}
            onError={() => {
                // Omit "Missing message" console error for missing translation.
                // Might not the best idea, but missing translations are nevertheless visible on UI.
                // And when getStaticPaths() for dynamic pages pre-generation, it inevitably hits this error while build-time, nonsense
            }}>
            <Layout>
                {/* Common items to put into <head> block: favicon, metatags etc. */}
                <Head>
                    <link rel="icon" type="image/x-icon" href={icon.src} />
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />

                    <HrefLangAlternate pathname={pathname} />
                </Head>
                <Component {...pageProps} />
            </Layout>
        </NextIntlClientProvider>
    );
}

function HrefLangAlternate({ pathname }: { pathname: string }) {

    return (
        <>
            {cfg.languages.map(item => {
                const url = new URL(cfg.publicUrl);
                url.pathname = item + pathname;

                return <link key={item} rel="alternate" hrefLang={item} href={url.toString()} />;
            })}
        </>
    );
}