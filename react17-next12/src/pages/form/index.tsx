import Head from "next/head";
import { useEffect, useRef } from "react";
import { useTranslations } from "next-intl";

import { getTranslations } from "@/helpers";
import { ILocalizationContext } from "@/interfaces";
import { useAsync } from "@/hooks/useAsync";
import { SubmitForm } from "@/services/PlaceholderService";

export async function getStaticProps(context: ILocalizationContext) {

    return {
        props: {
            ...await getTranslations(context.locale)
        }
    };
}

export default function FormPage() {

    const t = useTranslations();

    const form = useRef<HTMLFormElement>(null);
    const { action: submitAction, result, isProcessing, error } = useAsync(SubmitForm);

    // Clear form when loading is finished and no errors
    useEffect(() => {
        !isProcessing && !error && form.current?.reset();
    }, [isProcessing, error]);

    return (
        <>
            <Head>
                <title>Form</title>
            </Head>
            <section className="section">
                <h1>{t("Form.Title")}</h1>
                <p>{t("Form.Text")}</p>
                <form action="/api/form" onSubmit={submitAction} ref={form} className="form">
                    <label className="form__label">
                        <span>Some random value <br />
                            <i>(type &quot;error&quot; to rise a error)</i>
                        </span>
                        <input type="text" name="value" className="form__input"></input>
                    </label>
                    <button type="submit" disabled={isProcessing} className="form__button">Submit</button>
                </form>
                {isProcessing && <i>Loading...</i>}
                {!isProcessing && <p>{result}</p>}
                {error && <b>{error.toString()}</b>}
            </section>
        </>
    );
}