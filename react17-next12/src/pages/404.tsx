import Head from "next/head";

import { getTranslations } from "@/helpers";
import { ILocalizationContext } from "@/interfaces";

export async function getStaticProps(context: ILocalizationContext) {

    return {
        props: {
            ...await getTranslations(context.locale)
        }
    };
}

export default function NotFound() {
    return (
        <>
            <Head>
                <title>Not Found</title>
            </Head>
            <section className="section">
                <h1>Not Found</h1>
                <pre>¯\_(ツ)_/¯</pre>
                <p>This is fallback page</p>
            </section>
        </>
    );
}