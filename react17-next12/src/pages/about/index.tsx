import Head from "next/head";
import Link from "next/link";
import { useTranslations } from "next-intl";

import { getTranslations } from "@/helpers";
import { ILocalizationContext } from "@/interfaces";

export async function getStaticProps(context: ILocalizationContext) {

    return {
        props: {
            ...await getTranslations(context.locale)
        }
    };
}

export default function AboutPage() {

    const t = useTranslations();

    return (
        <>
            <Head>
                <title>{t("About.Title")}</title>
            </Head>
            <section className="section">
                <h1>{t("About.Title")}</h1>
                <p>{t("About.Text")}</p>
                <ul>
                    <li>Server-Side Rendering</li>
                    <li>
                        <Link href="https://nextjs.org/docs/pages/building-your-application/styling/sass">
                            <a target="_blank" className="link">Sass CSS preprocessor</a>
                        </Link>
                    </li>
                    <li>
                        <Link href="https://nextjs.org/docs/pages/building-your-application/data-fetching">
                            <a target="_blank" className="link">Data Fetching</a>
                        </Link>
                    </li>
                    <li>
                        <Link href="https://nextjs.org/docs/pages/building-your-application/routing/custom-error">
                            <a target="_blank" className="link">Error handling</a>
                        </Link>
                    </li>
                    <li>
                        <Link href="https://nextjs.org/docs/pages/building-your-application/routing/pages-and-layouts" >
                            <a target="_blank" className="link">Page Router</a>
                        </Link>
                    </li>
                    <li>
                        <Link href="https://nextjs.org/docs/pages/building-your-application/routing/api-routes">
                            <a target="_blank" className="link">API</a>
                        </Link>
                    </li>
                    <li>
                        <Link href="https://nextjs.org/docs/pages/building-your-application/routing/middleware">
                            <a target="_blank" className="link">Middleware</a>
                        </Link>
                    </li>
                    <li>
                        <Link href="https://nextjs.org/docs/pages/building-your-application/testing/vitest">
                            <a target="_blank" className="link">Testing</a>
                        </Link>
                    </li>
                    <li>
                        <Link href="https://next-intl-docs.vercel.app/docs/getting-started/pages-router">
                            <a target="_blank" className="link">Localization</a>
                        </Link>
                    </li>
                </ul>
            </section>
        </>
    );
}