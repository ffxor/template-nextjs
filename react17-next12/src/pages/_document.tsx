import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
    return (
        // html lang attribute is being added automatically
        <Html>
            <Head />
            <body>
                <Main />
                <NextScript />
            </body>
        </Html>
    );
}