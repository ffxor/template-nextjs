import cfg from "@/configuration";

export async function getTranslations(locale: string) {
    // If while build-time static pages pre-generation comes here with "default" locale, as it is specified to init redirection,
    // ...return for it default language
    if (locale === "default") {
        locale = cfg.defaultLanguage;
    }
    const translations = (await import(`@/../locales/${locale}/translation.json`)).default;

    return {
        translations
    };
} 