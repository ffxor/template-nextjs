import { useEffect } from "react";
import { IFetchResult, useAsync } from "./useAsync";

// Re-exporting for consistency of hook usage
export type { IFetchResult } from "./useAsync";

/**
 * Implementation of `useEffect` and `useAsync` hooks for data fetching
 * @param fetcher - Function retrieving a data
 * @returns 
 */
export function useFetch<T>(fetcher: (_signal: AbortSignal) => Promise<T>) {

    const { action, result, isProcessing, error } = useAsync(fetcher);

    useEffect(() => {
        action();
    }, [action]);

    return { result, isProcessing, error } as IFetchResult<T>;
}