import { useCallback, useMemo, useRef, useState, useEffect } from "react";

/**
 * Represents result of async call for data-fetching
 */
export interface IFetchResult<T> {
    /**
     * Result data
     */
    result: T,

    /**
     * Indicator whether fetching process in progress
     */
    isProcessing: boolean,

    /**
     * Object indicating an error if occurred
     */
    error?: unknown
}

/**
 * Represents result of async call for data-fetching withing `useAsync` hook
 */
export interface IUseAsyncResult<T> extends IFetchResult<T> {
    /**
     * Wrapper to call for a delegate(), which was passed into hook
     * @param _params
     * @returns 
     */
    action: (..._params: Array<any>) => Promise<T>,

    /**
     * `AbortController` to control operation cancellation
     */
    cancellation: AbortController,
}

/**
 * Implementation of state and result wrapper for client-side data fetching or any async function
 * @param delegate - Fetching data function, which is returned as callable `action()`
 * @returns 
 */
export function useAsync<T>(delegate: (_signal: AbortSignal, ..._params: Array<any>) => Promise<T>) {

    const [isProcessing, setIsProcessing] = useState(false);
    const [error, setError] = useState<unknown>(null);
    const [result, setResult] = useState<T | null>(null);

    const controller = useMemo(() => new AbortController(), []);

    const signal = controller.signal;

    // To use `isProcessing` only for unmount, wrap it as ref,
    // And not be dependent with useEffect() on `isProcessing`
    const isProcessingRef = useRef(isProcessing);
    useEffect(() => {
        isProcessingRef.current = isProcessing;
    }, [isProcessing]);

    // Perform aborting when component unmounted and deletage is running
    useEffect(() => {
        return () => {
            isProcessingRef.current && controller.abort();
        };
    }, [controller]);

    // Every time when component re-renders, instance of delegate() changes.
    // But we do not want to be dependent on it.
    // And do not create a new instance of action() on every render, thus wrapped in useCallback()
    const action = useCallback(
        // Action recieves the same parameters as rest of passed delegate()
        async (...params: Array<any>) => {

            setError(null);
            setResult(null);
            setIsProcessing(true);

            try {
                const data = await delegate(signal, ...params);
                !signal.aborted && setResult(data);
            } catch (err) {
                !signal.aborted && setError(err);
            } finally {
                !signal.aborted && setIsProcessing(false);
            }
        },
        // Skip here linting rule on purpose:
        // eslint-disable-next-line react-hooks/exhaustive-deps
        []);

    return {
        action,
        cancellation: controller,
        result, isProcessing, error
    } as IUseAsyncResult<T>;
}