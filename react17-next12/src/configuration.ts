const config = {
    timeZone: process.env.TZ,
    languages: process.env.NEXT_PUBLIC_LANGUAGES?.split(",") || [],
    defaultLanguage: process.env.NEXT_PUBLIC_DEFAULT_LANGUAGE || "en",
    publicUrl: process.env.NEXT_PUBLIC_URL || "http://localhost"
};

export default config;