import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";

import cfg from "@/configuration";

const PUBLIC_FILE_REGEX = /\.(.*)$/;
const API_PATH = "/api/";

export function middleware(request: NextRequest) {

    const nextUrl = request.nextUrl;

    // If we call /api or a static file, just proceed futher
    if (nextUrl.pathname.includes(API_PATH) || PUBLIC_FILE_REGEX.test(nextUrl.pathname)) {

        return NextResponse.next();
    }

    // Redirect to the proper locale URL, of locale wasn't specified
    if (nextUrl.locale === "default") {
        console.log("Locale redirection activated");

        const redirectUrl = new URL(`/${cfg.defaultLanguage}${nextUrl.pathname}${nextUrl.search}`, request.url);
        return NextResponse.redirect(redirectUrl);
    }

    return NextResponse.next();
}

/*export const config = {
    // Regex mather does no work for Next.js 12
    // https://nextjs.org/docs/pages/building-your-application/routing/middleware#matcher
    matcher: "/"
};*/