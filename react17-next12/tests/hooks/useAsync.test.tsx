import { describe, expect, test } from "vitest";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";

import { IUseAsyncResult, useAsync } from "@/hooks/useAsync";

describe("useAsync", () => {

    // Emulate running for async function
    const wait = () => new Promise(resolve => setTimeout(resolve, 10));

    const Element = ({ action, result, isProcessing, error }: IUseAsyncResult<string>) => {
        return (<>
            <button onClick={action}>Start</button>
            <div data-testid="loading">{isProcessing.toString()}</div>
            <div data-testid="result">{result}</div>
            <div data-testid="error">{error?.toString()}</div>
        </>);
    };

    // Helper for getting elements
    const getElements = () => {
        return {
            buttonEl: screen.getByRole("button"),
            loadingEl: screen.getByTestId("loading"),
            resultEl: screen.getByTestId("result"),
            errorEl: screen.getByTestId("error")
        }
    };

    test("Async function runs with indication", async () => {
        // Arrange
        const value = "data-value";


        const App = () => {
            const props = useAsync(async () => {

                // Emulate fetching
                await wait();
                return value;
            });

            return (<Element {...props} />);
        };

        // Act
        render(<App />);
        const { buttonEl, loadingEl, resultEl, errorEl } = getElements();

        // Assert Initial state
        expect(loadingEl.innerHTML).toMatch(false.toString());
        expect(resultEl.innerHTML).toMatch("");
        expect(errorEl.innerHTML).toMatch("");

        // Act
        fireEvent.click(buttonEl);

        // Assert Loading state
        await waitFor(() => {
            expect(loadingEl.innerHTML).toMatch(true.toString());
            expect(resultEl.innerHTML).toMatch("");
            expect(errorEl.innerHTML).toMatch("");
        });

        // Assert Comolete state
        await waitFor(() => {
            expect(loadingEl.innerHTML).toMatch(false.toString());
            expect(resultEl.innerHTML).toMatch(value);
            expect(errorEl.innerHTML).toMatch("");
        });
    });

    test("Async function error handled", async () => {
        // Arrange
        const errorObj = new Error("The error message");

        const App = () => {
            const props = useAsync(async () => {
                throw errorObj;
            });

            return (<Element {...props} />);
        };

        // Act
        render(<App />);
        const { buttonEl, loadingEl, resultEl, errorEl } = getElements();
        fireEvent.click(buttonEl);

        // Assert 
        await waitFor(() => {
            expect(loadingEl.innerHTML).toMatch(false.toString());
            expect(resultEl.innerHTML).toMatch("");
            expect(errorEl.innerHTML).toMatch(errorObj.toString());
        });
    });

    test("Delegate receives signal when aborting", async () => {
        // Arrange
        let isAborted = false;

        const App = () => {
            const { action, cancellation } = useAsync(async (signal) => {
                await wait();

                isAborted = signal.aborted;
                return "";
            });

            return (<>
                <button onClick={() => {
                    // Start action
                    action();
                    // Abort execution
                    cancellation.abort();
                }}>Cancel</button>
            </>);
        };

        // Act
        render(<App />);
        const buttonEl = screen.getByRole("button");
        fireEvent.click(buttonEl);
        // Need to wait to emulate parallel processes
        await wait();

        // Assert
        expect(isAborted).toEqual(true);
    });
});