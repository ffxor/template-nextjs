import { describe, expect, test } from "vitest";
import { render, screen, waitFor } from "@testing-library/react";

import { IFetchResult, useFetch } from "@/hooks/useFetch";

describe("useFetch", () => {

    // Emulate running for async function
    const wait = () => new Promise(resolve => setTimeout(resolve, 10));

    const Element = ({ result, isProcessing, error }: IFetchResult<string>) => {
        return (<>
            <div data-testid="loading">{isProcessing.toString()}</div>
            <div data-testid="result">{result}</div>
            <div data-testid="error">{error?.toString()}</div>
        </>);
    };

    // Helper for getting elements
    const getElements = () => {
        return {
            loadingEl: screen.getByTestId("loading"),
            resultEl: screen.getByTestId("result"),
            errorEl: screen.getByTestId("error")
        }
    };

    test("Data fetching happens with indication", async () => {
        // Arrange
        const value = "data-value";

        const App = () => {
            const props = useFetch(async () => {
                return value;
            });

            return (<Element {...props} />);
        };

        // Act
        render(<App />);

        // Assert
        const { loadingEl, resultEl, errorEl } = getElements();

        await waitFor(() => {
            expect(loadingEl.innerHTML).toMatch(true.toString());
            expect(resultEl.innerHTML).toMatch("");
            expect(errorEl.innerHTML).toMatch("");
        });

        await waitFor(() => {
            expect(loadingEl.innerHTML).toMatch(false.toString());
            expect(resultEl.innerHTML).toMatch(value);
            expect(errorEl.innerHTML).toMatch("");
        });
    });

    test("Data fetching error handled", async () => {
        // Arrange
        const errorObj = new Error("The error message");

        const App = () => {
            const props = useFetch(async () => {
                throw errorObj;
            });

            return (<Element {...props} />);
        };

        // Act
        render(<App />);

        // Assert
        const { loadingEl, resultEl, errorEl } = getElements();

        await waitFor(() => {
            expect(loadingEl.innerHTML).toMatch(true.toString());
            expect(resultEl.innerHTML).toMatch("");
            expect(errorEl.innerHTML).toMatch("");
        });

        await waitFor(() => {
            expect(loadingEl.innerHTML).toMatch(false.toString());
            expect(resultEl.innerHTML).toMatch("");
            expect(errorEl.innerHTML).toMatch(errorObj.toString());
        });
    });

    test("Fetcher receives signal when unmount", async () => {
        // Arrange
        let isAborted = false;

        const App = () => {
            useFetch(async (signal) => {
                await wait();

                isAborted = signal.aborted;
                return "";
            });

            return (<></>);
        };

        // Act
        const { unmount } = render(<App />);
        unmount();
        // Need to wait to emulate parallel processes
        await wait();

        // Assert
        expect(isAborted).toEqual(true);
    });
});