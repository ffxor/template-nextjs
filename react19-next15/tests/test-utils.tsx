import type { PropsWithChildren, ReactElement } from "react";
import { NextIntlClientProvider } from "next-intl";
import { render } from "@testing-library/react";

import messages from "../locales/en/translation.json";

/**
 * Make custom rendering of element wrapped with necessary providers
 * @param ui Element to be rendered
 * @param param ComponentProps typeof IntlProvider
 */
export function renderWithProviders(ui: ReactElement, { locale = "en", timeZone = "UTC", ...renderOptions } = {}) {

    function providers({ children }: PropsWithChildren) {

        return (
            <NextIntlClientProvider locale={locale} timeZone={timeZone} messages={messages}>
                {children}
            </NextIntlClientProvider>
        );
    }

    return render(ui, { wrapper: providers, ...renderOptions });
}