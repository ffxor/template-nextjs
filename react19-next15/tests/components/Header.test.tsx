import { describe, test, vi } from "vitest";
import { renderWithProviders } from "../test-utils";

import { Header } from "@/components/current";

// Inside Header there is a components with `useRouter()`, need to mock the hook
vi.mock("next/router", () => ({

    useRouter() {
        return {
            route: "/",
            pathname: "",
            query: "",
            asPath: "",
        };
    }
}));

describe("Header", () => {

    //? Info: Example of component rendering for testing with providers

    test("Renders", () => {
        // Act
        renderWithProviders(<Header />);

        console.log(document.body.innerHTML);
    });
});