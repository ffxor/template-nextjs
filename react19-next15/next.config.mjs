import { join, dirname } from "path";
import { fileURLToPath } from "url";
import { readFileSync, existsSync } from "fs";

// Normalize NODE_ENV to be "development | production", there is no need to create extra "test" config files
const nodeEnv = () => process.env.NODE_ENV !== "test" ? process.env.NODE_ENV : "development";

const __dirname = dirname(fileURLToPath(import.meta.url));

const isLocal = process.env.IS_LOCAL ?? false;
const localEnvConfig = isLocal ? readLocalEnvConfiguration() : {};
const languages = process.env.NEXT_PUBLIC_LANGUAGES?.split(",");

console.info({ isLocal });

/** @type {import('next').NextConfig} */
const nextConfig = {
    distDir: "dist",
    poweredByHeader: false,
    // Configure the Sass compiler
    sassOptions: {
        includePaths: [join(__dirname, "src/styles")]
    },
    // Crucial to add for locale being redirected from "/" (aka "default") to "/en" to work
    trailingSlash: true,
    // Extra eslint options
    eslint: {
        dirs: ["src", "tests"]
    },
    env: {
        // Overrides channel config if necessary
        ...localEnvConfig
    },
    ...(languages && {
        i18n: {
            locales: ["default", ...languages],
            // To redirect from "/" to "/en" in middleware
            // https://nextjs.org/docs/pages/building-your-application/routing/internationalization#prefixing-the-default-locale
            defaultLocale: "default",
            localeDetection: false
        }
    })
}

export default nextConfig;

/**
 * Read `.env.{ENVIRONMENT}.local` configuration
 * @description The file is being read automatically, but it's needed to re-read it to override channel configuration
 * @returns Object with configuration
 */
function readLocalEnvConfiguration() {

    const NEWLINE = /\r?\n/g;
    const localEnvConfig = {};
    const path = join(__dirname, `./.env.${nodeEnv()}.local`);

    if (!existsSync(path)) {
        return localEnvConfig;
    }

    const text = readFileSync(path, "utf-8");;
    const lines = text.split(NEWLINE);
    for (let i = 0; i < lines.length; i++) {
        const line = lines[i];
        // If line is empty, or doesn't contain `=` or it's a comment
        if (!line || !line.indexOf("=") || line.startsWith("#")) {
            continue;
        }

        const [name, value] = line.split("=");
        localEnvConfig[name] = value;
    }

    console.info(`Loaded env configuration: ${path}`);

    return localEnvConfig;
}