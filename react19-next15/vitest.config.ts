import { resolve } from "path";
import { loadEnv } from "vite";
import { defineConfig } from "vitest/config";
import react from "@vitejs/plugin-react";

import nextConfig from "./next.config.mjs";

export default defineConfig({
    plugins: [react()],
    resolve: {
        alias: {
            // Module Path Aliases
            "@": resolve(__dirname, "./src"),
        }
    },
    test: {
        // make all variables available in test files
        globals: true,
        // set the environment to jsdom, which is the default environment for Vitest
        environment: "jsdom",
        // clear mock history and reset its implementation to the original one
        restoreMocks: true,
        env: {
            // Load .env-files
            ...loadEnv("development", process.cwd(), ""),
            // Load channel configuration
            ...nextConfig.env
        }
    }
});