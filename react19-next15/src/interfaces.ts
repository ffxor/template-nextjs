import type { NextPage } from "next";
import type { AbstractIntlMessages } from "next-intl";
import { ReactElement, ReactNode } from "react";

/**
 * Default props collection for each component
 */
export interface IComponentProps {
    /**
     * Provide classname to be set from parent component
     */
    className?: string;
}

/**
 * Default props collection for each page
 */
export interface IPageProps {
    /**
     * Url of requested page
     * @description Only when `getServerSideProps()` is utilized
     */
    requestUrl?: string;
    /**
     * Translations source
     */
    translations: AbstractIntlMessages;
}

/**
 * Provide function returning specified layout to override for particular page
 * @see https://nextjs.org/docs/pages/building-your-application/routing/pages-and-layouts#with-typescript
 */
export type PageWithLayout<TProps = object, TInitialProps = TProps> =
    NextPage<TProps, TInitialProps> & {
        getNestedLayout?: (page: ReactElement) => ReactNode;
    }