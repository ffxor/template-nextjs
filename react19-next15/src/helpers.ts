import type { GetServerSidePropsContext, GetStaticPropsContext, GetStaticPathsResult } from "next/types";

import type { IPageProps } from "@/interfaces";
import cfg from "@/configuration";

/**
 * Fetch server-side props for a page based on the request and locale
 * @param context The context object provided by Next.JS
 * @description Tip: use either `getServerSideProps` or `getStaticProps`
 */
export async function getServerSideProps({ req, locale }: GetServerSidePropsContext) {

    // Get from nginx proxy headers
    const scheme = req.headers["x-forwarded-proto"]?.toString() || "http";
    const host = req.headers["x-forwarded-host"]?.toString() || "localhost";

    const props: IPageProps = {
        requestUrl: `${scheme}://${host}${req.url}`,
        ...await getTranslations(locale)
    };

    return {
        props
    };
}

/**
 * Fetch static props for a page based on the locale
 * @param context The context object provided by Next.JS
 * @description Tip: use either `getServerSideProps` or `getStaticProps`
 * @example
    // Example of usage to obtain data before loading page
    import type { GetStaticPropsContext, InferGetStaticPropsType } from "next/types";
    import { getStaticProps as getSp } from "@/helpers";

    export async function getStaticProps(context: GetStaticPropsContext) {
        const { props } = await getSp(context);
        const authenticationMethods = await getAuthenticationMethods();
        return {
            props: {
                ...props,
                authenticationMethods
            }
        };
    }
    function LoginPage({ authenticationMethods }: InferGetStaticPropsType<typeof getStaticProps>) 
 */
export async function getStaticProps(context: GetStaticPropsContext) {

    // When_SSR: is uses `context.locale`, otherwise - `context.params.locale`
    const locale = context.locale ?? context.params?.["locale"]?.toString() ?? cfg.defaultLanguage;
    const props: IPageProps = {
        ...await getTranslations(locale)
    };

    return {
        props
    };
}

/**
 * Fetch all static paths for a page according to possible locales
 */
export function getStaticPaths(): GetStaticPathsResult {

    return {
        fallback: false,
        paths: getI18nPaths()
    };
}

function getI18nPaths() {

    return cfg.languages.map(language => ({
        params: {
            locale: language
        }
    }));
}

async function getTranslations(locale: string = "default") {

    // If while build-time static pages pre-generation comes here with "default" locale, as it is specified to init redirection,
    // ...return for it default language
    if (locale === "default") {
        locale = cfg.defaultLanguage;
    }
    const translations = (await import(`@/../locales/${locale}/translation.json`)).default;

    return {
        translations
    };
}