const config = {
    // Indicate local launch of the app
    isLocal: process.env.IS_LOCAL ?? false,
    isDevelopment: process.env.NODE_ENV === "development",
    environment: process.env.NEXT_PUBLIC_ENVIRONMENT,
    timeZone: process.env.TZ,

    languages: process.env.NEXT_PUBLIC_LANGUAGES?.split(",") ?? [],
    defaultLanguage: process.env.NEXT_PUBLIC_DEFAULT_LANGUAGE ?? "en",

    bffUrl: process.env.BFF_URL!,

    gtmId: process.env.NEXT_PUBLIC_GTM_ID
};

export default config;