import type { NextRequest } from "next/server";
import { NextResponse } from "next/server";

import cfg from "@/configuration";

const PUBLIC_FILE_REGEX = /\.(.*)$/;
const GITLAB_PATH_REGEX = /^\/users\/.*/;   // Proxy to api/v4/ as example
const API_PATH = "/api/";
const PROXY_PATH = "/api/proxy";

export function middleware(request: NextRequest) {

    const { pathname, locale, search } = request.nextUrl;

    // If we call local `/api` or a static file, just proceed further
    if (pathname.startsWith(API_PATH) || PUBLIC_FILE_REGEX.test(pathname)) {
        return NextResponse.next();
    }

    if (GITLAB_PATH_REGEX.test(pathname)) {
        const newUrl = new URL(PROXY_PATH + pathname + search, request.url);
        console.info(`Rewrite URL from '${request.nextUrl}' to: '${newUrl.toString()}'`);
        return NextResponse.rewrite(newUrl);
    }

    // Redirect to the proper locale URL, of locale wasn't specified
    if (locale === "default") {
        const redirectUrl = new URL(`/${cfg.defaultLanguage}${pathname}${search}`, request.url);
        console.info(`Locale redirection to: '${redirectUrl}'`);
        return NextResponse.redirect(redirectUrl);
    }

    return NextResponse.next();
}