import type { ClientRequest, IncomingMessage, ServerResponse } from "http";
import type { NextApiRequest, NextApiResponse } from "next";

import { createProxyMiddleware } from "http-proxy-middleware";

import cfg from "@/configuration";

const proxy = createProxyMiddleware({
    target: cfg.bffUrl,
    secure: false, //! Only for development
    changeOrigin: true,
    pathRewrite: {
        // Remove `/api/proxy` prefix
        "^/api/proxy": ""
    },
    on: {
        proxyReq: (proxyReq: ClientRequest, _request: IncomingMessage, _response: ServerResponse<IncomingMessage>) => {
            // These headers are useful when you use it on nginx proxy
            // But here it might affect the result from target. It removed for seamless integration
            proxyReq.removeHeader("x-forwarded-proto");
            proxyReq.removeHeader("x-forwarded-for");
            proxyReq.removeHeader("x-forwarded-host");
        },
        proxyRes: (proxyRes: IncomingMessage, _request: IncomingMessage, _response: ServerResponse<IncomingMessage>) => {
            // For debugging purposes we log the result
            console.log("statusCode", proxyRes.statusCode);
            console.log("headers", proxyRes.headers);
        }
    },
});

/**
 * API with catch-all route for proxying requests from `/api/proxy/bff` to `<BFF_URL>/bff`
 * @description 
 * Such implementation is needed, because Next.js cannot verify self-signed TLS certificates, 
 * when proxying with `rewrites` in config or `NextResponse.rewrite()` in middleware.
 * @see https://github.com/vercel/next.js/discussions/49546
 */
export default function handler(request: NextApiRequest, response: NextApiResponse) {

    const { url } = request;
    console.info(`Proxy to BFF: '${cfg.bffUrl}${url}'`);

    proxy(request, response, (err) => {
        if (err) {
            throw err;
        }
        throw new Error(`Request '${url}' is not proxied`);
    });
}

export const config = {
    api: {
        // To consume the body as a stream without parsing
        bodyParser: false,
        // Tells the server that this route is being handled by an external resolver
        // Disables warnings for unresolved requests
        externalResolver: true
    }
};