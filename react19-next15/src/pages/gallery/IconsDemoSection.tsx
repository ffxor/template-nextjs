import { TelegramIcon, WhatsappIcon } from "@/components/icons";
import { ChevronIcon } from "@/components/icons";

import s from "./index.module.scss";

export default function IconsDemoSection() {

    return (
        <section className="section">
            <h2>Icons</h2>
            <article className={s.gallery__collection}>

                <fieldset className={s.gallery__collection__icons}>
                    <legend>UI</legend>

                    <ChevronIcon />
                </fieldset>

                <fieldset className={s.gallery__collection__icons}>
                    <legend>Social</legend>

                    <TelegramIcon />
                    <WhatsappIcon />
                </fieldset>

            </article>
        </section>
    );
}