import { Input } from "@/components/form";

import s from "./index.module.scss";

export default function InputDemoSection() {

    return (
        <section className="section">
            <h2>Input</h2>
            <article className={s.gallery__collection}>

                <Input placeholder="Default" />
                <Input placeholder="Disabled" disabled />
                <Input helperText="Helper text" />
                <Input label="Label" />
                <Input label="Error" helperText="Error field message" error />

            </article>
        </section>
    );
}