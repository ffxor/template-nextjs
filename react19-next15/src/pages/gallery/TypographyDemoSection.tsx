import s from "./index.module.scss";

export default function TypographyDemoSection() {

    return (
        <section className="section">
            <h2>Typography</h2>
            <article className={s.gallery__collection}>

                <div>
                    <h1 className="heading_x-large">Header 1</h1>
                    <h2 className="heading_large">Header 2</h2>
                    <h3 className="heading_medium">Header 3</h3>
                    <h4 className="heading_small">Header 4</h4>
                </div>

                <div>
                    <p className="paragraph_large">Large paragraph</p>
                    <p className="paragraph_medium">Medium paragraph</p>
                    <p className="paragraph_small">Small paragraph</p>
                </div>

            </article>
        </section>
    );
}