import { Checkbox, Radio } from "@/components/form";
import s from "./index.module.scss";

export default function CheckboxAndRadioDemoSection() {

    return (
        <section className="section">
            <h2>Checkbox &amp; Radio</h2>
            <article className={s.gallery__collection}>

                <div>
                    <Checkbox name="checkbox-default" value="1" label="Default Option" />
                    <Checkbox name="checkbox-readonly" value="2" label="Readonly Option Checked" checked readOnly />
                    <Checkbox name="checkbox-disabled" value="3" label="Disabled Option" disabled />
                    <Checkbox name="checkbox-disabled" value="4" label="Disabled Option Checked" disabled checked />
                    <Checkbox name="checkbox-error" value="5" label="Error Option" error />
                </div>

                <div role="radiogroup">
                    <Radio name="radio" value="info" label="Info Option" />
                    <Radio name="radio" value="success" label="Success Option" defaultChecked />
                    <Radio name="radio" value="warning" label="Warning Option" />
                    <Radio name="radio" value="error" label="Error Option" error />
                    <Radio name="radio" value="" label="Disabled Option" disabled />
                </div>

            </article>
        </section>
    );
}