import { Button } from "@/components/form";
import s from "./index.module.scss";

export default function ButtonsDemoSection() {

    return (
        <section className="section">
            <h2>Buttons</h2>
            <article className={s.gallery__collection}>

                <Button variant="contained">Filled Button</Button>
                <Button variant="contained" disabled>Filled Button Disabled</Button>

                <Button variant="outlined">Tonal Button</Button>
                <Button variant="outlined" disabled>Tonal Button Disabled</Button>

            </article>
        </section>
    );
}