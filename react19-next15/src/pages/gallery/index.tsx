import type { ReactElement } from "react";
import type { GetStaticPropsContext, InferGetStaticPropsType } from "next/types";
import { useTranslations } from "next-intl";
import Head from "next/head";

import { Details } from "@/components/current";
import { SimpleLayout } from "@/pages/layout";
import { getStaticProps as getSp } from "@/helpers";
import ButtonsDemoSection from "./ButtonsDemoSection";
import CheckboxAndRadioDemoSection from "./CheckboxAndRadioDemoSection";
import IconsDemoSection from "./IconsDemoSection";
import InputDemoSection from "./InputDemoSection";
import TypographyDemoSection from "./TypographyDemoSection";
import cfg from "@/configuration";

/**
 * Utilize imported `getStaticProps()` and append to it
 */
export async function getStaticProps(context: GetStaticPropsContext) {

    const { props } = await getSp(context);

    return {
        props,
        notFound: !cfg.isDevelopment
    };
}

// Here example how to use these overridden props
export default function GalleryPage(props: InferGetStaticPropsType<typeof getStaticProps>) {

    const t = useTranslations("gallery");

    return (
        <>
            <Head>
                <title>{t("title")}</title>
            </Head>

            <h1 className="heading_x-large">{t("title")}</h1>

            <section className="section">
                <Details {...props} />
            </section>

            <section className="section">
                <p>&#128073; {t("availableOnly")} <code>NODE_ENV === development</code></p>
                <p>&#128073; {t("separatedLayout")} <code>&lt;footer&gt;</code></p>
            </section>

            <ButtonsDemoSection />
            <hr />
            <CheckboxAndRadioDemoSection />
            <hr />
            <InputDemoSection />
            <hr />
            <TypographyDemoSection />
            <hr />
            <IconsDemoSection />
        </>
    );
}

// To omit regular layout
GalleryPage.getNestedLayout = (page: ReactElement) => (
    <SimpleLayout>
        {page}
    </SimpleLayout>
);