import { PropsWithChildren } from "react";
import { useLocale } from "next-intl";
import Head from "next/head";

import type { IPageProps } from "@/interfaces";
import { Footer, Header, SkipToMain } from "@/components/current";
import { GoogleTagManager } from "@/components/scripts/GoogleTagManager";
import cfg from "@/configuration";

export default function Layout({ requestUrl, children }: IPageProps & PropsWithChildren) {

    const locale = useLocale();

    return (
        <>
            <Head>
                <meta charSet="UTF-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />

                <link rel="icon" type="image/svg" href="/favicon.svg" />
                <link rel="shortcut icon" href="/favicon.ico" />

                {requestUrl && <>
                    {hrefLangAlternate(requestUrl, locale)}
                    {openGraph(requestUrl, locale)}
                </>}
            </Head>

            <WhenJavaScriptDisabled />
            <EmbeddedScripts />

            <div className="container">
                <SkipToMain />
            </div>
            {children}
        </>
    );
}

/**
 * Default layout user for all pages
 */
export function RegularLayout({ children }: PropsWithChildren) {

    return (
        <>
            <Header />
            <main id="main">
                <div className="container">
                    {children}
                </div>
            </main>
            <Footer />
        </>
    );
}

/**
 * Just the same layout but without footer
 */
export function SimpleLayout({ children }: PropsWithChildren) {
    return (
        <>
            <Header />
            <main id="main">
                <div className="container">
                    {children}
                </div>
            </main>
        </>
    );
}

/**
 * Add scripts which are needed to embed into the page
 */
function EmbeddedScripts() {

    // strategy="beforeInteractive" - renders script to html <head>
    // strategy="afterInteractive" (default) - will put script into html <body>
    return (
        <>
            {cfg.gtmId && <GoogleTagManager strategy="beforeInteractive" />}
        </>
    );
}

/**
 * Placeholder to show when JS is disabled in user's browser
 */
function WhenJavaScriptDisabled() {

    return (
        <>
            <style>{`
                .no-js {
                    display: block;
                    background-color: coral;
                    color: #FFF;
                    text-align: center;
                }
            `}</style>
            <noscript className="no-js">Java-script is disabled</noscript>
        </>
    );
}

/**
 * Generate alternate-links for multilingual pages
 * @param url Request url
 * @param locale Current locale
 */
function hrefLangAlternate(url: string, locale: string) {

    return (
        <>
            {cfg.languages.filter(item => item !== locale).map(item => {
                const alternateUrl = new URL(url);
                alternateUrl.pathname = item + alternateUrl.pathname;
                return <link key={item} rel="alternate" hrefLang={item} href={alternateUrl.toString()} />;
            })}
        </>
    );
}

/**
 * Add OpenGraph info to page
 * @param url Request url
 * @param locale Current locale
 */
function openGraph(url: string, locale?: string) {

    const ogUrl = new URL(url);
    ogUrl.pathname = locale + ogUrl.pathname;

    return (
        <>
            <meta property="og:title" content="Page title for SEO" />
            <meta property="og:description" content="Page description for SEO" />
            <meta property="og:url" content={ogUrl.toString()} />
        </>
    );
}