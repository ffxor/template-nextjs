import type { DocumentContext } from "next/document";
import Document, { Html, Head, Main, NextScript } from "next/document";

// Class component is used to access props
class Doc extends Document {

    static async getInitialProps(context: DocumentContext) {

        const initialProps = await Document.getInitialProps(context);

        return {
            ...initialProps,
            // Put extra styles into document header if needed
            styles: (
                <>
                    {initialProps.styles}
                </>
            ),
        };
    }

    render() {
        // When_SSR: html lang attribute is being added automatically, but When_SSG:
        // const currentLocale = this.props.__NEXT_DATA__.query.locale?.toString();
        // <Html lang={currentLocale}>
        return (
            <Html>
                <Head />
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}

export default Doc;