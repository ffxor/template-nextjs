export { getStaticProps } from "@/helpers";

export default function NotFound() {

    return (
        <section>
            <h1>Not Found</h1>
            <pre>¯\_(ツ)_/¯</pre>
            <p>This is fallback page</p>
        </section>
    );
}