import { useTranslations } from "next-intl";
import Head from "next/head";

import type { IPageProps } from "@/interfaces";
import { Details } from "@/components/current";

export { getServerSideProps } from "@/helpers";

// `props` here passed by App
export default function Page(props: IPageProps) {

    const t = useTranslations();
    const hlo = t("hello", {
        name: "Next.JS"
    });

    return (
        <>
            <Head>
                <title>{hlo}</title>
            </Head>

            <h1 className="heading_x-large">{hlo}</h1>

            <section className="section">
                <Details {...props} />
            </section>

            <section className="section">
                <ul className="list paragraph_medium">
                    <li><a href="https://nextjs.org/docs/pages/building-your-application/rendering/server-side-rendering" target="_blank" className="link">Server-Side-Rendering</a></li>
                    <li><a href="https://nextjs.org/docs/pages/building-your-application/routing/pages-and-layoutss" target="_blank" className="link">Pages Router</a></li>
                    <li><a href="https://nextjs.org/docs/pages/building-your-application/routing/pages-and-layouts#per-page-layouts" target="_blank" className="link">Per-page layouts</a></li>
                    <li><a href="https://nextjs.org/docs/pages/building-your-application/routing/custom-error" target="_blank" className="link">Custom errors</a></li>
                    <li><a href="https://nextjs.org/docs/pages/building-your-application/routing/middleware" target="_blank" className="link">Middleware</a></li>
                    <li><a href="https://nextjs.org/docs/pages/building-your-application/routing/api-routes" target="_blank" className="link">API</a></li>
                    <li><a href="https://nextjs.org/docs/pages/building-your-application/styling/sass" target="_blank" className="link">Sass CSS preprocessor</a></li>
                    <li><a href="https://next-intl.dev/docs/getting-started/pages-router" target="_blank" className="link">Localization</a></li>
                    <li><a href="https://nextjs.org/docs/pages/api-reference/components/script" target="_blank" className="link">Scripts</a></li>
                    <li><a href="https://nextjs.org/docs/pages/api-reference/config/eslint" target="_blank" className="link">ESLint</a></li>
                </ul>
            </section>
        </>
    );
}