import type { AppProps } from "next/app";
import { useRouter } from "next/router";
import { NextIntlClientProvider } from "next-intl";

import type { IPageProps, PageWithLayout } from "@/interfaces";
import Layout, { RegularLayout } from "@/pages/layout";
import cfg from "@/configuration";

import "@/styles/index.scss";

type AppPropsWithLayout<T> = AppProps<T> & {
    Component: PageWithLayout;
}

export default function App({ Component, pageProps }: AppPropsWithLayout<IPageProps>) {

    // When_SSR: Receiving locale at both cases: SSR and static site accordingly
    const { locale, query } = useRouter();
    const detectedLocale = locale ?? query["locale"]?.toString() ?? cfg.defaultLanguage;

    const getNestedLayout = Component.getNestedLayout ?? ((page) => (
        // Default regular layout for all pages not having overrides
        <RegularLayout>{page}</RegularLayout>
    ));

    return (
        <NextIntlClientProvider locale={detectedLocale} timeZone={cfg.timeZone} messages={pageProps.translations}
            onError={onTranslationError}>

            <Layout {...pageProps}>
                {getNestedLayout(
                    <Component {...pageProps} />
                )}
            </Layout>

        </NextIntlClientProvider>
    );
}

function onTranslationError() {
    // Omit "Missing message" console error for missing translation.
    // Might not the best idea, but missing translations are nevertheless visible on UI.
    // And when getStaticPaths() for dynamic pages pre-generation, it inevitably hits this error while build-time, nonsense
}