import type { ScriptProps } from "next/script";
import Script from "next/script";

import cfg from "@/configuration";

/**
 * Placeholder for GoogleTagManager script
 */
export function GoogleTagManager(props: ScriptProps) {

    return (
        <>
            {/* Initialize the dataLayer */}
            <Script id="dataLayer-init" {...props}>
                {`
                    window.dataLayer = window.dataLayer || [];
                `}
            </Script>
            <Script id="gtm" {...props}>
                {`
// <!-- Google Tag Manager -->
// ${cfg.gtmId}
//<!-- End Google Tag Manager -->
            `}
            </Script>
        </>
    );
}