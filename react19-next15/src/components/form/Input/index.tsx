import type { InputHTMLAttributes } from "react";
import { useId } from "react";

import type { IInputProps } from "..";
import { FormLabel } from "@/components/form";

import cn from "classnames";
import s from "./Input.module.scss";

export function Input({ type = "text", label, helperText, error, className, ...rest }: IInputProps & InputHTMLAttributes<HTMLInputElement>) {

    // For now there is only one variant
    const variant = "outlined";

    const hintId = `helper-${useId()}`;
    const attributes = {
        ...helperText && { "aria-describedby": hintId },
        ...error && { "aria-invalid": true }
    };

    return (
        <div className={cn(s["form-input"], className)}>
            <FormLabel position="top" label={label}>
                <input type={type} className={cn(s["form-input__input"], s[`form-input_${variant}`], className)} {...rest} {...attributes} />
            </FormLabel>
            {helperText && <span id={hintId} className={s["form-input__helper"]}>{helperText}</span>}
        </div>
    );
}