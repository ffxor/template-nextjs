import type { ButtonHTMLAttributes } from "react";

import type { IFormElementProps } from "..";

import cn from "classnames";
import s from "./Button.module.scss";

export function Button({ variant, className, children, ...rest }: Pick<IFormElementProps, "variant"> & ButtonHTMLAttributes<HTMLButtonElement>) {

    return (
        <button className={cn(s["form-button"], s[`form-button_${variant}`], className)} {...rest}>
            {children}
        </button>
    );
}