import type { InputHTMLAttributes } from "react";

import type { ICheckboxRadioProps } from "..";
import { FormLabel } from "@/components/form";

import cn from "classnames";
import s from "./CheckboxRadio.module.scss";

export function Checkbox({ label, error, className, ...rest }: ICheckboxRadioProps & InputHTMLAttributes<HTMLInputElement>) {

    const attributes = {
        ...error && { "aria-invalid": true }
    };

    return (
        <FormLabel position="right" label={label} className={cn(s["form-choice__label"], className)}>
            <input type="checkbox" className={cn(s["form-choice__element"], s["form-checkbox"], className)} {...rest} {...attributes} />
        </FormLabel>
    );
}

export function Radio({ label, error, className, ...rest }: ICheckboxRadioProps & InputHTMLAttributes<HTMLInputElement>) {

    const attributes = {
        ...error && { "aria-invalid": true }
    };

    return (
        <FormLabel position="right" label={label} className={cn(s["form-choice__label"], className)}>
            <input type="radio" className={cn(s["form-choice__element"], s["form-radio"], className)} {...rest} {...attributes} />
        </FormLabel>
    );
}