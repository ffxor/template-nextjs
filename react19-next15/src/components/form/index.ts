import type { ReactNode } from "react";

import type { IComponentProps } from "@/interfaces";

export interface IFormElementProps extends IComponentProps {
    variant?: "contained" | "outlined";
    error?: boolean;
}

export interface ILabelProps {
    position: "left" | "right" | "top";
    label: ReactNode;
}

export interface IInputProps extends Pick<IFormElementProps, "error">, Partial<Pick<ILabelProps, "label">> {
    helperText?: string;
}

export interface ICheckboxRadioProps extends Pick<IFormElementProps, "error">, Partial<ILabelProps> { }

export * from "./Button";
export * from "./CheckboxRadio";
export * from "./FormLabel";
export * from "./Input";