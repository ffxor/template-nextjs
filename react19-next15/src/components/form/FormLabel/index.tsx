import type { LabelHTMLAttributes } from "react";

import type { ILabelProps } from "..";

import cn from "classnames";
import s from "./FormLabel.module.scss";

export function FormLabel({ position, label, className, children, ...rest }: ILabelProps & LabelHTMLAttributes<HTMLLabelElement>) {

    if (label) {
        return (
            <label className={cn(s["form-label"], s[`form-label_${position}`], className)} {...rest}>
                <span className={s["form-label__text"]}>{label}</span>
                {children}
            </label>
        );
    }

    return (<>{children}</>);
}