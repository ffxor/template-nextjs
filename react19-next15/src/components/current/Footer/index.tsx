import type { IComponentProps } from "@/interfaces";

import cn from "classnames";
import s from "./Footer.module.scss";

export function Footer({ className }: IComponentProps) {

    return (
        <footer className={cn(s.footer, className)}>
            <div className={cn("container", s.footer__row)}>
                <p className="heading_large">&#128640;</p>
                <pre>&copy;&nbsp;2025</pre>
            </div>
        </footer>
    );
}