import { useLocale, useTranslations } from "next-intl";

import type { IComponentProps, IPageProps } from "@/interfaces";
import cfg from "@/configuration";

import cn from "classnames";
import s from "./Details.module.scss";

export function Details({ className, requestUrl }: IComponentProps & IPageProps) {

    const t = useTranslations("details");
    const locale = useLocale();

    return (
        <details open className={cn(s.details, className)}>
            <summary><h2 className={cn("heading_medium", s.details__title)}>{t("title")}</h2></summary>

            <pre>isLocal: {cfg.isLocal.toString()}</pre>
            <pre>isDevelopment: {cfg.isDevelopment.toString()}</pre>
            <pre>environment: {cfg.environment}</pre>
            <pre>locale: {locale.toUpperCase()}</pre>
            <pre>requestUrl: {requestUrl}</pre>
            <p className="paragraph_small">&#128296; {t("uses")} {requestUrl ? <code>getServerSideProps()</code> : <code>getStaticProps()</code>}</p>
        </details>
    );
}