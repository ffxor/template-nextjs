import { useRouter } from "next/router";
import { useLocale } from "next-intl";
import Link from "next/link";

import type { IComponentProps } from "@/interfaces";

import cn from "classnames";
import s from "./LanguageSelector.module.scss";

export interface ILanguageSelectorProps extends IComponentProps {
    languages: Array<string>;
}

export function LanguageSelector({ className, languages }: ILanguageSelectorProps) {

    const locale = useLocale();
    const router = useRouter();

    // When_SSR: leave the func empty, no need for href
    const prepareLocaleLink = (locale: string) => router?.route.replace("[locale]", locale) ?? "";

    return (
        <nav className={cn(s["language-selector"], className)}>
            <ul className={s["language-selector__list"]}>

                {languages.map(item => {
                    if (item === locale) {
                        return (
                            <li key={item} className={cn(s["language-selector__list__item"], s["language-selector__list__item_active"])}>
                                {locale}
                            </li>
                        );
                    }

                    return (
                        <li key={item}>
                            <Link href={prepareLocaleLink(item)} locale={item} hrefLang={item} className={cn(s["language-selector__list__item"])}>
                                {item}
                            </Link>
                        </li>
                    );
                })}

            </ul>
        </nav>
    );
}