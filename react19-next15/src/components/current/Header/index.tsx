import { useTranslations } from "next-intl";
import Link from "next/link";

import type { IComponentProps } from "@/interfaces";
import { LanguageSelector } from "@/components/current";
import cfg from "@/configuration";

import cn from "classnames";
import s from "./Header.module.scss";

export function Header({ className }: IComponentProps) {

    const t = useTranslations("menu");

    return (
        <header className={cn(s.header, className)}>
            <div className={cn("container", s.header__row)}>
                <nav className={s.header__menu}>
                    <Link href="/" className="link">{t("main")}</Link>
                    <Link href="/gallery" className="link">{t("gallery")}</Link>
                </nav>
                <LanguageSelector languages={cfg.languages} className={cn(s.header__language)} />
            </div>
        </header>
    );
}