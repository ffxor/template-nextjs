import { useTranslations } from "next-intl";

import type { IComponentProps } from "@/interfaces";

import cn from "classnames";
import s from "./SkipToMain.module.scss";

export function SkipToMain({ className }: IComponentProps) {

    const t = useTranslations();

    return (
        <a href="#main" className={cn(s["skip-to-main"], "link", className)}>&#128071;&nbsp;{t("skipToMain")}</a>
    );
}