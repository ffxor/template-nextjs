// UI
export { default as ChevronIcon } from "./ui/Chevron";

// Social
export { default as TelegramIcon } from "./social/Telegram";
export { default as WhatsappIcon } from "./social/Whatsapp";