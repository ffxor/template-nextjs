import { join, dirname } from "path";
import { fileURLToPath } from "url";

const __dirname = dirname(fileURLToPath(import.meta.url));

const nextConfig = {
    distDir: "dist",
    poweredByHeader: false,
    // Configure the Sass compiler
    sassOptions: {
        includePaths: [join(__dirname, "src/styles")]
    }
}

export default nextConfig;