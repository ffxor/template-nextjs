import Link from "next/link";
import cn from "classnames";

import styles from "./Header.module.scss";
import { NavLink } from "@/components/NavLink";

export function Header() {

    return (
        <header className={cn(styles.header)}>
            <nav>
                <ul className={cn(styles.header__menu)}>
                    <li><Link href="/" className="link">Home</Link></li>
                    <li><NavLink href="/about" title="Sample page for routing">About</NavLink></li>
                    <li><NavLink href="/blog" title="Dynamic routing example">Blog</NavLink></li>
                    <li><NavLink href="/parallel" title="Combined page example">Parallel</NavLink></li>
                    <li><NavLink href="/photos" title="Combined page approach with photo gallery">Photos</NavLink></li>
                    <li><NavLink href="/form" title="Form submit example">Form</NavLink></li>
                    <li><NavLink href="/api" target="_blank" title="API example">API</NavLink></li>
                    <li><NavLink href={new Date().getDate().toString()} title="Random link to display Not Found">Not Found</NavLink></li>
                </ul>
            </nav>
        </header>
    );
}