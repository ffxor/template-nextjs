"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";

export function NavLink({ href, children, ...rest }: React.AnchorHTMLAttributes<HTMLAnchorElement>) {
    const path = usePathname();

    return (
        <Link href={href || "#"}
            className={ new RegExp(`^${href}\/?`).test(path) ? "link link_active" : "link"}
            {...rest}>{children}</Link>
    );
}