"use client";

import { ChangeEvent, useRef, useState } from "react";
import cn from "classnames";

import styles from "./ImagePicker.module.scss";

type ImagePickerProps = {
    name: string
}

export function ImagePicker({ name }: ImagePickerProps) {

    const [pickedImage, setPickedImage] = useState<string | null>(null);
    const imageInput = useRef<HTMLInputElement>(null);

    const clickPickImage = () => imageInput.current?.click();

    function changeImage({ target }: ChangeEvent<HTMLInputElement>) {
        const file = target?.files?.item(0);

        if (!file) {
            setPickedImage(null);
            return;
        }

        const fr = new FileReader();
        fr.onload = () => setPickedImage(fr.result?.toString() ?? null);
        fr.readAsDataURL(file);
    }

    return (
        <div className={cn(styles.picker)}>
            <label className="form__label">
                <span>Pick some image</span>
                <input type="file" name={name} accept="image/png, image/jpeg" onChange={changeImage} ref={imageInput} />
                <button type="button" onClick={clickPickImage} className="form__button">Pick image</button>
            </label>
            <div className={cn(styles.picker__preview)}>
                {pickedImage ? <img src={pickedImage} alt="Selected image" /> : <p>No image picked</p>}
            </div>
        </div>
    );
}