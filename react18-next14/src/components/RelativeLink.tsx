"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";

export function RelativeLink({ href, children, ...rest }: React.AnchorHTMLAttributes<HTMLAnchorElement>) {
    const path = usePathname();

    return (
        <Link href={`${path}/${href}`} className="link" {...rest}>{children}</Link>
    );
}