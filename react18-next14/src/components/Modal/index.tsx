"use client";

import { PropsWithChildren, type ElementRef, useEffect, useRef } from "react";
import { useRouter } from "next/navigation";
import cn from "classnames";

import styles from "./Modal.module.scss";

export function Modal({ children }: PropsWithChildren) {
    const router = useRouter();
    const dialog = useRef<ElementRef<"dialog">>(null);

    useEffect(() => {
        if (!dialog.current?.open) {
            dialog.current?.showModal();
        }
    }, []);

    function onDismiss() {
        router.back();
    }

    return (
        <div className={cn(styles["modal-backdrop"])}>
            <dialog ref={dialog} className={cn(styles.modal)} onClose={onDismiss}>
                {children}
                <button onClick={onDismiss} className={cn(styles["close-button"])} />
            </dialog>
        </div>
    );
}