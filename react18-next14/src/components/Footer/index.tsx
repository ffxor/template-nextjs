import cn from "classnames";

import styles from "./Footer.module.scss";

export function Footer() {
    return (
        <footer className={cn(styles.footer)}>
            <p>Footer</p>
        </footer>
    );
}