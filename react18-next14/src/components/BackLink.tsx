import Link from "next/link";
import { PropsWithChildren } from "react";

export function BackLink({ children }: PropsWithChildren) {
    return (
        <Link href="." className="link">{children}</Link>
    );
}