import logo from "@/assets/logo.svg";

export default function Page() {
    
    return (
        <section className="section">
            <img src={logo.src} width="128" height="128" className="logo" alt="Next.js logo" />
            <h1>Hello, Next.js!</h1>
            <p>🔥 Let&apos;s get started! 🔥</p>
            <p>{process.env.NEXT_PUBLIC_VALUE}</p>
        </section>
    );
}