"use client";

import { SubmitPost, TActionReturn } from "@/services/PlaceholderService";
import { useEffect, useRef } from "react";
import { useFormState, useFormStatus } from "react-dom";

import { ImagePicker } from "@/components/ImagePicker";

export default function Form() {

    const form = useRef<HTMLFormElement>(null);

    const initialState: TActionReturn = {
        message: "",
        type: "idle",
    };
    const [formState, formAction] = useFormState(SubmitPost, initialState);

    useEffect(() => {
        form.current?.reset();
    }, [formState]);

    return (
        <section className="section">
            <h1>Form</h1>
            <p>This is an example of form submitting handler and image picker preview</p>
            <form className="form" action={formAction} ref={form}>
                <fieldset className="form__field">
                    <label className="form__label">
                        <span>Some random value <br />
                            <i>(leave empty to rise a error)</i>
                        </span>
                        <input type="text" name="value" className="form__input"></input>
                    </label>
                    <ImagePicker name="file" />
                </fieldset>
                <FormSubmitButton className="form__button">Submit</FormSubmitButton>
                <SubmitResult formState={formState} />
            </form>
        </section>
    );
}

function FormSubmitButton({ children, ...rest }: React.ButtonHTMLAttributes<HTMLButtonElement>) {

    // Getting status of submitting the nearest form
    const { pending } = useFormStatus();

    return (
        <button type="submit" {...rest} disabled={pending}>{children}</button>
    );
}

function SubmitResult({ formState }: { formState: TActionReturn }) {

    // Getting status of submitting the nearest form
    const { pending } = useFormStatus();

    return (
        <div>
            {
                !pending && {
                    ["error"]: <pre>Error: {formState.message}</pre>,
                    ["success"]: <pre>Submitted: {formState.message}</pre>,
                    ["idle"]: <p></p>
                }[formState.type]
            }
        </div>
    );
}