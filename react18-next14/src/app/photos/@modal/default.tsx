export default function Default() {
    // That's required to not produce an error, when accessing /photos/<id> URL
    return null;
}