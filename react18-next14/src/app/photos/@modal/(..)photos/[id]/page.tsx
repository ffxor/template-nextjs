import { IdProps } from "@/app/photos/[id]/page";
import { Modal } from "@/components/Modal";

export default function PhotoModal({ params: { id } }: IdProps) {
    return (
        <Modal>
            {id}
        </Modal>
    );
}