import cn from "classnames";
import { Metadata } from "next";

import styles from "../Photos.module.scss";

export type IdProps = {
    params: {
        // Corresponds to folder name
        id: string
    }
};

export async function generateMetadata({ params: { id } }: IdProps) {
    return {
        title: `Photos - ${id}`
    } as Metadata;
}


export default function PhotoPage({ params: { id } }: IdProps) {
    return (
        <article>
            <h2>Photo - {id}</h2>
            <div className={cn(styles.card)}>{id}</div>
        </article>
    );
}