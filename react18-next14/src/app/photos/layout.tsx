export default function PhotoLayout({ children, modal }: {
    children: React.ReactNode;
    // Corresponds @modal folter name
    modal: React.ReactNode;
}) {
    return (
        <>
            {children}
            {modal}
        </>
    );
}