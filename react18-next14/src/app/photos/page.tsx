import Link from "next/link";
import { Metadata } from "next";
import cn from "classnames";

import styles from "./Photos.module.scss";

export const metadata: Metadata = {
    title: "Photos"
};

export default function Photos() {
    // Dummy photos
    const photos = Array.from({ length: 6 }, (_, i) => i + 1);

    return (
        <section className="section">
            <h1>Photos</h1>
            <p>This is an example of usage Parallel page routing with route interceptor to create an image gallery.</p>
            <p>A card when clicked will open as modal, but if to open the card URL directly (or refresh with modal), it&apos;ll be opened as regular page.</p>
            <article className={cn(styles["cards-container"])}>
                {photos.map((id) => (
                    <Link className={cn(styles.card)} key={id} href={`/photos/${id}`}>{id}</Link>
                ))}
            </article>
        </section>
    );
}