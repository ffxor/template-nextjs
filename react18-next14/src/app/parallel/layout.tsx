import { Metadata } from "next";
import React from "react";

export const metadata: Metadata = {
    title: "Parallel pages"
};

export default function Parallel({ blogPart, defaultPart }: {
    blogPart: React.ReactNode,
    defaultPart: React.ReactNode,
}) {
    return (
        <>
            <section className="section">
                <h1>Parallel pages</h1>
                <p>This is example of a combined page displaying two sub-pages in parallel:</p>
                <h2>😎 Parallel Part 1</h2>
                <p>Reused Blog. Through parameters, it can receive any the same as the original page.</p>
                <p>&#8681;</p>
            </section>
            {blogPart}
            <hr />
            <section className="section">
                <h2>🧐 Parallel Part 2</h2>
                <p>&#8681;</p>
            </section>
            {defaultPart}
        </>
    );
}