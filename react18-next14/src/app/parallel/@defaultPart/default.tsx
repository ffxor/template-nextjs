export default function DefaultPart() {
    return (
        <section className="section">
            <p>Let it be a secondary content.</p>
            <p>Displays independently on the route the primary content is having</p>
        </section>
    );
}