import { Metadata } from "next";

import BlogPost from "@/app/blog/[slug]/page";
import Blog from "@/app/blog/page";

type FilterProps = {
    params: {
        // Corresponds to folder name
        filter: string[]
    }
};

export async function generateMetadata({ params: { filter } }: FilterProps) {

    const slug = filter?.[0];
    const title = !slug ? "Parallel pages" : `Parallel Blog post - ${slug}`;

    return {
        title
    } as Metadata;
}

export default function BlogPart({ params: { filter } }: FilterProps) {

    const slug = filter?.[0];

    return (
        <>
            {!slug
                ? <Blog />
                : <BlogPost params={{
                    slug: slug
                }} />
            }
        </>
    );
}