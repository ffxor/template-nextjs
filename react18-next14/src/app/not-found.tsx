import { Metadata } from "next";

export const metadata : Metadata = {
    title: "Not Found"
};

export default function NotFound() {
    return (
        <section className="section">
            <h1>Not Found</h1>
            <pre>¯\_(ツ)_/¯</pre>
            <p>This is fallback page</p>
        </section>
    );
}