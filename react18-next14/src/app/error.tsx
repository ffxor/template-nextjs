"use client";

export default function Error() {
    return (
        <section className="section">
            <h1>An error happened</h1>
        </section>
    );
}