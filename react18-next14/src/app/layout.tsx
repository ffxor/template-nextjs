import { PropsWithChildren } from "react";
import { Metadata } from "next";

import { Header } from "@/components/Header";
import { Footer } from "@/components/Footer";

import "@/styles/index.scss";

export const metadata: Metadata = {
    title: "Hello Next 14 application",
    description: "This is page description for SEO"
};

export default function RootLayout({ children }: PropsWithChildren) {
    return (
        <html lang="en">
            <body>
                <Header />
                <main>{children}</main>
                <Footer />
            </body>
        </html>
    );
}