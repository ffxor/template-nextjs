import { Metadata } from "next";
import { notFound } from "next/navigation";
import { Suspense } from "react";

import { BackLink } from "@/components/BackLink";
import { GetPost } from "@/services/PlaceholderService";

type SlugProps = {
    params: {
        // Corresponds to folder name
        slug: string
    }
};

export async function generateMetadata({ params: { slug } }: SlugProps) {
    return {
        title: `Blog post - ${slug}`
    } as Metadata;
}

export default function BlogPost({ params: { slug } }: SlugProps) {

    return (
        <section className="section">
            <h1>Blog post: {slug}</h1>
            <Suspense fallback={<i>Loading post...</i>}>
                <Post slug={slug} />
            </Suspense>
            <p></p>
            <BackLink>Back</BackLink>
        </section>
    );
}

async function Post({ slug }: { slug: string }) {

    const post = await GetPost(slug);

    if (!post) {
        // Shows the closest not-found page
        notFound();
    }

    return (
        <article>
            <h2>{post.title}</h2>
            <p>{post.body}</p>
        </article>
    );
}