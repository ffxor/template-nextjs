import { Metadata } from "next";
import { Suspense } from "react";

import { GetAllPosts } from "@/services/PlaceholderService";
import { RelativeLink } from "@/components/RelativeLink";

export const metadata: Metadata = {
    title: "Blog"
};

export default function Blog() {
    return (
        <section className="section">
            <h1>Blog</h1>
            <p>This is an example of dynamic routing and fetching content from <a href="https://jsonplaceholder.typicode.com" target="_blank" className="link">jsonplaceholder.typicode.com</a></p>
            <Suspense fallback={<i>Loading...</i>}>
                <Posts />
            </Suspense>
        </section>
    );
}

async function Posts() {
    
    const value = process.env.CONNECTION_STRING;
    console.log(value);

    const posts = await GetAllPosts();    

    return (
        <ul>
            {posts.map((post) => (
                <li key={post.id}>
                    <RelativeLink href={post.id}>{post.title}</RelativeLink>
                </li>
            ))}
        </ul>
    );
}