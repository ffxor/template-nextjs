import { Metadata } from "next";
import Link from "next/link";

export const metadata: Metadata = {
    title: "About"
};

export default function AboutPage() {
    return (
        <section className="section">
            <h1>About</h1>
            <p>This is an example of Next.js 14 TypeScript project including:</p>
            <ul>
                <li>Server-Side Rendering</li>
                <li><Link href="https://nextjs.org/docs/app/building-your-application/styling/sass" target="_blank" className="link">Sass CSS preprocessor</Link></li>
                <li><Link href="https://nextjs.org/docs/app/building-your-application/data-fetching/fetching-caching-and-revalidating" target="_blank" className="link">Data Fetching</Link></li>
                <li>Form submitting</li>
                <li>Image pick preview</li>
                <li><Link href="https://nextjs.org/docs/app/building-your-application/routing/error-handling" target="_blank" className="link">Error handling</Link></li>
                <li><Link href="https://nextjs.org/docs/app/building-your-application/routing/defining-routes" target="_blank" className="link">App Router</Link></li>
                <li><Link href="https://nextjs.org/docs/app/building-your-application/routing/parallel-routes" target="_blank" className="link">Parallel Routes (multiple pages in one)</Link></li>
                <li><Link href="https://nextjs.org/docs/app/building-your-application/routing/intercepting-routes" target="_blank" className="link">Intercepting Routes (modal/regular opening content)</Link></li>
                <li><Link href="https://nextjs.org/docs/app/building-your-application/routing/route-handlers" target="_blank" className="link">API</Link></li>
                <li><Link href="https://nextjs.org/docs/app/building-your-application/routing/middleware" target="_blank" className="link">Middleware</Link></li>
            </ul>
        </section>
    );
}