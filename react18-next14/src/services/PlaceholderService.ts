"use server";

export type Post = {
    id: string,
    title: string,
    body: string
};

export type TActionState = "idle" | "error" | "success";

export type TActionReturn = {
    message: string;
    type: TActionState;
};

export async function GetAllPosts(): Promise<Post[]> {

    const URL = "https://jsonplaceholder.typicode.com/posts";

    // A synthetic delay for loader element to be shown
    await new Promise(resolve => setTimeout(resolve, 1000));

    const response = await fetch(URL);
    const posts: Post[] = (await response.json()).slice(0, 10); // Take first 10 elements for brevity

    return posts;
}

export async function GetPost(id: string): Promise<Post | null> {

    const URL = `https://jsonplaceholder.typicode.com/posts/${id}`;

    // A synthetic delay for loader element to be shown
    await new Promise(resolve => setTimeout(resolve, 1000));

    const response = await fetch(URL);
    if (response.status === 404) {
        return null;
    }
    const post: Post = await response.json();

    return post;
}

export async function SubmitPost(_: TActionReturn, formData: FormData): Promise<TActionReturn> {

    // A synthetic delay for loader element to be shown
    await new Promise(resolve => setTimeout(resolve, 1000));

    const value = formData.get("value");
    const file = formData.get("file") as File;
    // Kinda server data validation
    if (value === undefined || value === "") {
        return {
            message: "Empty value",
            type: "error"
        };
    }

    return {
        message: `Value=${value}; FileSize=${file?.size ?? 0}`,
        type: "success"
    };
}