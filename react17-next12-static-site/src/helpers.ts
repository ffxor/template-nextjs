import type { GetStaticPathsResult, GetStaticPropsResult, GetStaticPropsContext } from "next/types";

import type { IPageProps } from "./interfaces";

/**
 * Fetches static props for a page based on the locale
 *
 * @param {GetStaticPropsContext} context - The context object provided by Next.js
 * @returns {Promise<GetStaticPropsResult<IPageProps>>}
 */
export async function getStaticProps(context: GetStaticPropsContext): Promise<GetStaticPropsResult<IPageProps>> {

    const locale = context.params?.locale?.toString() ?? "en";
    const props: IPageProps = {
        ...await getTranslations(locale)
    };

    return {
        props
    };
}

/**
 * Fetches all static paths for a page according to possible locales
 * 
 * @returns {GetStaticPathsResult}
 */
export function getStaticPaths(): GetStaticPathsResult {

    return {
        fallback: false,
        paths: getI18nPaths()
    };
}

function getI18nPaths() {

    return ["en", "ru"].map(language => ({
        params: {
            locale: language
        }
    }));
}

async function getTranslations(locale: string = "default") {

    // If while build-time static pages pre-generation comes here with "default" locale, as it is specified to init redirection,
    // ...return for it default language
    if (locale === "default") {
        locale = "en";
    }
    const translations = (await import(`@/../locales/${locale}/translation.json`)).default;

    return {
        translations
    };
}