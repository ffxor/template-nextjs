import type { ReactNode } from "react";
import type { AbstractIntlMessages } from "next-intl";

/**
 * Default props collection for each component
 */
export interface IComponentProps {
    // Provides classname to be set from parent component
    className?: string;
    children?: ReactNode;
}

/**
 * Default props collection for each page
 */
export interface IPageProps {
    /**
     * Url of requested page (only when `getServerSideProps()` used)
     */
    requestUrl?: string;
    /**
     * Translations source
     */
    translations: AbstractIntlMessages;
}