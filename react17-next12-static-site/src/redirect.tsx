import { useEffect } from 'react'
import { useRouter } from 'next/router'
//import languageDetector from './languageDetector'

export function useLocaleRedirect(to?: string) {
    
    const router = useRouter();
    to = to || router.asPath;
    console.log(router.route)

    // language detection
    useEffect(() => {
        
        const detectedLng = "en";
        if (to.startsWith("/" + detectedLng) && router.route === "/404") {
            // prevent endless loop
            router.replace("/" + detectedLng + router.route);
            return;
        }

        //languageDetector.cache(detectedLng)
        router.replace("/" + detectedLng + to);
    })
}