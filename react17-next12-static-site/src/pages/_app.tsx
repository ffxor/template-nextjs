import type { AppProps } from "next/app";
import { useRouter } from "next/router";
import Head from "next/head";
import { NextIntlClientProvider } from "next-intl";
import Layout from "./layout";

import type { IPageProps } from "@/interfaces";

export default function MyApp({ Component, pageProps }: AppProps & { pageProps: IPageProps }) {

    const router = useRouter();

    return (
        <NextIntlClientProvider locale={router.query["locale"]?.toString() ?? "en"} timeZone={process.env.TZ} messages={pageProps.translations}
            onError={() => {
                // Omit "Missing message" console error for missing translation.
                // Might not the best idea, but missing translations are nevertheless visible on UI.
                // And when getStaticPaths() for dynamic pages pre-generation, it inevitably hits this error while build-time, nonsense
            }}>
            <Layout>
                <Head>
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                </Head>
                <Component />
            </Layout>
        </NextIntlClientProvider>
    );
}
