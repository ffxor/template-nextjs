import { useLocaleRedirect } from '../redirect'

export default function IndexPage() {
    useLocaleRedirect()
    return <></>
}