import type { PropsWithChildren, ReactNode } from "react";
import Head from "next/head";

export default function Layout({ children }: PropsWithChildren<ReactNode>) {

    return (
        <>
            <Head>
                <title>Hello static site</title>
            </Head>
            <header>HEADER</header>
            <main>{children}</main>
            <footer>FOOTER</footer>
        </>
    );
}