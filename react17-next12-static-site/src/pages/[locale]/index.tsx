import { useTranslations } from "next-intl";
import Link from "next/link";

import { getStaticPaths, getStaticProps } from "../../helpers";

export { getStaticPaths, getStaticProps };

export default function Page() {

    const t = useTranslations();

    const onClick = () => {
        alert("It works!");
    }

    return (
        <section className="section">
            <p>This is static site</p>
            <p>{t("hello")}</p>
            <Link href="/about">About</Link>
            <Link href="/en" locale="en">EN</Link>
            <Link href="/ru" locale="ru">RU</Link>
            <button onClick={onClick}>Click</button>
        </section>
    );
}
