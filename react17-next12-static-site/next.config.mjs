/** @type {import('next').NextConfig} */
const nextConfig = {
    distDir: "dist",
    poweredByHeader: false,
    /*i18n: {
        locales: ["default", "en", "ru"],
        // To redirect from "/" to "/en" in middleware
        // https://nextjs.org/docs/pages/building-your-application/routing/internationalization#prefixing-the-default-locale
        defaultLocale: "default",
        localeDetection: false,
    },*/
    // Crucial to add for locale being redirected from "/" (aka "default") to "/en" to work
    trailingSlash: true
}

export default nextConfig;