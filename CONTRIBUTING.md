# Contributing guidelines

:wave: Hey, cool developer!

To ensure this and future created project's longevity and maintainability there a some guidelines preferable to follow. Consistent and reliable code is paramount to the project's success and our collective satisfaction :smiley:

## Components

### Folders structure

Components are conventionally housed within the `/src/components` directory. A hierarchical structure is employed to facilitate the eventual extraction of common components for integration into other frontend projects.

- **Current:** Contains components related to the template visuals.
- **Scripts:** Serves as a repository for scripts embedded within the page, including those for Google Tag Manager and similar services.
- **Icons:** For enhanced clarity, icons are categorized internally based on semantics: `social`, `ui` symbols.

Each subfolder in Components directory has its own `index.ts` file reexporting put inside components.

### Creating a component

All components should accept an `IComponentProps` interface or a derived interface as input. The root element of every component wants to incorporate a `className` property, potentially dynamically assigned through values passed from the encompassing page via the `IComponentProps` interface.

### Gallery of components

There is the special page `/gallery`, representing visuals and examples of usage for majority of components. It would be good to add a section into `/gallery` for a newly created component :wink:

### Creating a page

To facilitate the translation process, every page must re-export or implement Next.js dedicated functions:

```typescript
// (Option 1) For Server-Side-Rendering (SSR)
export { getServerSideProps } from "@/helpers";

// (Option 2) For Static-Site-Generation (SSG)
export { getStaticPaths, getStaticProps } from "@/helpers";
```

Employ these methods shown above for every page within your Next.js application, ensuring dynamic data fetching for optimal user experiences.

## Imports

### Order of imports

Adhering to a standardized import order across all source code can significantly enhance code readability and maintainability. Consider adopting the following import structure as a template.

```javascript
// Imports from frameworks (like react, next)
import type { PropsWithChildren, ReactNode } from "react";
import { useState, useContext, useEffect } from "react";

// Imports from external libs
import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";

// Imports from project with absolute path and then with relative path
import type { IComponentProps } from "@/interfaces";
import { ShowDemoButton } from ".";

// Import of styles
import cn from "classnames";
import s from "./Component.module.scss";

## Styling

### BEM methodology

CSS styles are structured using the [BEM methodology](https://www.toptal.com/css/introduction-to-bem-methodology) (Block Element Modifier) to enhance code maintainability within complex projects. This naming convention fosters clarity and organization by delineating components into distinct blocks, elements, and modifiers. Consider the following example:

```scss
.menu {
    display: flex;

    &__item {
        color: red;
        
        &_active {
            background-color: coral;
        }
    }
}
```

```html
<!-- CSS class names with following structure: `block__element_modifier` -->
<ol class="menu">
    <li class="menu__item">Item 1</li>
    <li class="menu__item menu__item_active">Item 2</li>
</ol>
```

### classnames 

The [classnames](https://www.npmjs.com/package/classnames) library is extensively utilized within our development process. It simplifies the application of CSS classes to elements, enhancing code readability and maintainability.

```css
/* index.module.scss */
.gallery__collection {
    /* ... */
}
```

```jsx
import cn from "classnames";
import s from "./index.module.scss";

export function Component({ className }: IComponentProps) {
    return (
        <div className={cn(s.gallery__collection, className)}></div>
    )
}

```

## Coding

- To enhance code readability and consistency, consider utilizing Visual Studio Code's built-in formatting feature. By pressing `Alt+Shift+F`, you can automatically align code indentation and spacing, improving overall code aesthetics.
- Upon feature completion:
  - Execute the `npm run lint` command to enforce code style guidelines and proactively identify potential code quality issues;
  - Execute the `npm run test` command to reassure, that nothing was broken, or add new necessary tests covering your feature.

:tada: **Happy coding!**